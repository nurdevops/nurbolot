terraform {
  # backend "s3" {
  #   bucket = "bucket-name"
  #   key    = "terraform/dev/"
  #   region = "us-east-1"
  # }
  required_providers {
    aws = {
      version = ">= 5.17.0"
      source  = "hashicorp/aws"
    }
  }
}
