#!/bin/bash

#installing kubectl
curl -o kubectl https://amazon-eks.s3.us-west-2.amazonaws.com/1.21.2/2021-07-05/bin/linux/amd64/kubectl
chmod +x ./kubectl
sudo cp ./kubectl /usr/local/bin
export PATH=/usr/local/bin:$PATH
curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
unzip awscliv2.zip
sudo ./aws/install

#installing git
sudo yum install git -y

#installing postgres
sudo amazon-linux-extras install epel -y
sudo yum repolist
sudo tee /etc/yum.repos.d/pgdg.repo<<EOF
[pgdg14]
name=PostgreSQL 14 for RHEL/CentOS 7 - x86_64
baseurl=http://download.postgresql.org/pub/repos/yum/14/redhat/rhel-7-x86_64
enabled=1
gpgcheck=0
EOF
sudo yum makecache -y
sudo yum install postgresql14 postgresql14-server -y

# echo 'installing helm'
curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3
chmod +x get_helm.sh
./get_helm.sh
helm list



### gitlab_runner
sudo echo 'installing gitlab_runner'
curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.rpm.sh" | sudo bash
sudo yum install gitlab-runner -y
sudo gitlab-runner status
sudo touch /etc/sudoers.d/gitlab-runner && sudo chmod 0440 /etc/sudoers.d/gitlab-runner
sudo echo 'gitlab-runner ALL=(ALL:ALL) ALL' >> sudo /etc/sudoers.d/gitlab-runner
sudo echo 'gitlab-runner ALL=(ALL) NOPASSWD: ALL' >> sudo /etc/sudoers.d/gitlab-runner
gitlab-runner register \
  --non-interactive \
  --url "https://gitlab.com" \
  --registration-token "GR13489413idGrek4VAxBGyjfhrKk" \
  --description "docker-ec2" \
  --tag "nurbolot" \
  --executor "docker" \
  --docker-image "docker:20.10.16" \
  --docker-privileged \
  --docker-volumes "/certs/client"
usermod -aG docker gitlab-runner
sudo gitlab-runner start



echo "db_endpoint = ${db_endpoint}" > /home/sentos/hello.txt
echo 'create ns and secret'
kubectl create ns nurbolot
kubectl create secret generic postgresnur -n nurbolot --from-literal=password="\$(echo -n '${dbpassword}' | base64)" --from-literal=user="\$(echo -n '${dbusername}' | base64)" --from-literal=db="\$(echo -n 'postgres' | base64)" --from-literal=host="\$(echo -n '${db_endpoint}' | base64)"
echo 'create tables'
psql -U ${dbusername} -h ${db_endpoint} -d postgres < /home/sentos/db.sql



